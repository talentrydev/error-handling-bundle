<?php

declare(strict_types=1);

namespace Talentry\ErrorHandlingBundle\Tests\Subscriber;

use PHPUnit\Framework\Attributes\WithoutErrorHandler;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Talentry\ErrorHandling\Error\Notice;
use Talentry\ErrorHandling\Error\Warning;

class ErrorHandlerEventSubscriberTest extends KernelTestCase
{
    private EventDispatcherInterface $dispatcher;
    private ConsoleCommandEvent $consoleCommandEvent;
    private ConsoleTerminateEvent $consoleTerminateEvent;
    private RequestEvent $requestEvent;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        restore_error_handler(); //symfony registers an error handler that will interfere with the tests

        /** @var ContainerInterface $container */
        $container = self::getContainer();
        $this->dispatcher = $container->has('event_dispatcher') ?
            $container->get('event_dispatcher') :
            $container->get(EventDispatcherInterface::class)
        ;
        $command = new Command('foo');
        $command->setApplication(new Application());
        $this->consoleCommandEvent = new ConsoleCommandEvent($command, new ArrayInput([]), new NullOutput());
        $this->consoleTerminateEvent = new ConsoleTerminateEvent($command, new ArrayInput([]), new NullOutput(), 0);
        $this->requestEvent = new RequestEvent(self::$kernel, new Request(), HttpKernelInterface::MASTER_REQUEST);
    }

    #[WithoutErrorHandler]
    public function testWithConsoleCommand(): void
    {
        $this->dispatcher->dispatch($this->consoleCommandEvent, ConsoleEvents::COMMAND);
        $this->assertWarningsAndNoticesAreCaught();
        //triggering a console terminate event will cause the error handler to be de-registered;
        //if we don't de-register it, phpunit will complain
        $this->dispatcher->dispatch($this->consoleTerminateEvent, ConsoleEvents::TERMINATE);
    }

    #[WithoutErrorHandler]
    public function testWithKernelRequest(): void
    {
        $this->dispatcher->dispatch($this->requestEvent, KernelEvents::REQUEST);
        $this->assertWarningsAndNoticesAreCaught();
        //triggering a kernel terminate event will cause the error handler to be de-registered;
        //if we don't de-register it, phpunit will complain
        $this->dispatcher->dispatch($this->consoleTerminateEvent, KernelEvents::TERMINATE);
    }

    private function assertWarningsAndNoticesAreCaught(): void
    {
        $caught = 0;
        try {
            $this->triggerNotice();
        } catch (Notice $notice) {
            $caught++;
        }

        try {
            $this->triggerWarning();
        } catch (Warning $warning) {
            $caught++;
        }

        self::assertSame(2, $caught);
    }

    private function triggerNotice(): void
    {
        (object) [1] == 1;
    }

    private function triggerWarning(): void
    {
        $s = 1;
        $s[1];
    }
}
