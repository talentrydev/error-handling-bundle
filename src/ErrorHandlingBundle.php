<?php

declare(strict_types=1);

namespace Talentry\ErrorHandlingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ErrorHandlingBundle extends Bundle
{
}
