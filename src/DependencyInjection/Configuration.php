<?php

declare(strict_types=1);

namespace Talentry\ErrorHandlingBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    private const ROOT_NODE = 'error_handling';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);
        $treeBuilder
            ->getRootNode()
            ->children()
            ->booleanNode('auto_start')->defaultValue(false)->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
