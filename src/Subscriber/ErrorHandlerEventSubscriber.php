<?php

declare(strict_types=1);

namespace Talentry\ErrorHandlingBundle\Subscriber;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\ErrorHandler;

class ErrorHandlerEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ErrorHandler $errorHandler,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest', 0],
            ],
            KernelEvents::TERMINATE => [
                ['onKernelTerminate', 0],
            ],
            ConsoleEvents::COMMAND => [
                ['onConsoleCommand', 0],
            ],
            ConsoleEvents::TERMINATE => [
                ['onConsoleTerminate', 0],
            ],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if ($event->isMainRequest()) {
            $this->startErrorHandler();
        }
    }

    public function onKernelTerminate(): void
    {
        $this->stopErrorHandler();
    }

    public function onConsoleCommand(): void
    {
        $this->startErrorHandler();
    }

    public function onConsoleTerminate(): void
    {
        $this->stopErrorHandler();
    }

    private function startErrorHandler(): void
    {
        $this->errorHandler->startHandling(Severity::WARNING, Severity::NOTICE);
    }

    private function stopErrorHandler(): void
    {
        $this->errorHandler->stopHandling();
    }
}
