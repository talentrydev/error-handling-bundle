# Error Handling Symfony bundle

This Symfony bundle encapsulates the [error-handling library](https://gitlab.com/talentrydev/error-handling).
Compatible with Symfony versions 3 and 4.

## Installing and configuring the symfony bundle

* Run:

```
composer require talentrydev/error-handling-bundle
```

* Add the ErrorHandlingBundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\ErrorHandlingBundle\ErrorHandlingBundle();
];
```

* Configure the bundle to automatically start error handling on kernel.request and console.command events:

```
error_handling:
    auto_start: true
```